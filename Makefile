# Copy all tfm files into the directory of the main.tex file
#copy-tfm:
#	 @find ./fonts -type f -iname "*.tfm" | while read -r file; do cp $$file .; done

#all: copy-tfm
#	@latexmk
#	@rm -f *.tfm	
		
#show: all
#	@evince main.pdf
	
clean:
	@latexmk -c -quiet
	@rm -f *.synctex*
	@rm -f *.tfm	

clean-all: clean
	@latexmk -C -quiet

